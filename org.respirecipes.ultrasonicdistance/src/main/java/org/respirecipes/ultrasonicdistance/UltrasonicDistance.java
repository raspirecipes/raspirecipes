package org.respirecipes.ultrasonicdistance;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

public class UltrasonicDistance {

	private static final int MEDIAN = 5;

	static Logger logger = LoggerFactory.getLogger(UltrasonicDistance.class);

	public static void main( String[] args ) throws InterruptedException
	{
		GpioController gpio = GpioFactory.getInstance();

		GpioPinDigitalOutput trigger = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02);
		trigger.setMode(PinMode.DIGITAL_OUTPUT);
		trigger.setState(PinState.LOW);

		GpioPinDigitalInput echo = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00);
		echo.setMode(PinMode.DIGITAL_INPUT);

		BlockingQueue<Double> distances = new ArrayBlockingQueue<Double>(10);
			
		AtomicLong startTime = new AtomicLong();
		AtomicLong stopTime = new AtomicLong();;

		// attach a listener to the echo that is called whenever pin state changes
		echo.addListener((GpioPinListenerDigital) event -> {
			if(event.getState().isHigh())
				startTime.set(System.nanoTime());
			else if(event.getState().isLow() && startTime.get() != 0) {
				stopTime.set(System.nanoTime());
				long echoTime = stopTime.get() - startTime.get();
				double centimeters = echoTime * 0.000017150;
				// throw away faulty measurements 
				if(centimeters > 2 && centimeters < 300) 
					// try to put proper measurements into the queue
					if(!distances.offer(centimeters)) {
						logger.warn("Queue full! Draining distances!");
						distances.clear();
					}
				stopTime.set(0);
				startTime.set(0);
			}
		});
		
		// start a Thread to take the measured distances and log them
		Thread distanceLogger = new Thread((Runnable)()-> {
			try {
				Median median = new Median();
				List<Double> medianSet = new ArrayList<Double>();
				while(true) {
					medianSet.add(distances.take());
					if(medianSet.size() == MEDIAN) {
						double[] vals = ArrayUtils.toPrimitive(medianSet.stream().toArray(Double[]::new));
						medianSet.clear();
						logger.info("Distance is {} cm", median.evaluate(vals));
					}		
				}
			}
			catch(InterruptedException e) {
				logger.error("Interrupt!");
			}
		});
		distanceLogger.start();

		// start an endless loop to measure with 50Hz (maximum of what the sensor can provide)
		while(true) {
			Thread.sleep(15);
			trigger.setState(PinState.LOW);
			Thread.sleep(0, 20000);
			trigger.setState(PinState.HIGH);
		}	
	}
}
